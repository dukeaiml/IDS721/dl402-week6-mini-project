# IDS 721 Mini Project 6
This project is to Create a Rust AWS Lambda function that have the ability to monitor and produce logs. 

## Packages and Dependencies
- Include tracing and tracing-subscriber to the packages.
- Enable AWS X-Ray by going to the configuration console after deploying the lambda function. Enable X-Ray active tracing. Also, for this project, create a new IAM role which have the following permissions: `AWSLambda_FullAccess`, `AWSXRayDaemonWriteAccess` and `IAMFullAccess`.

## Build and deploy the project:
- Run `cargo new week6` to create a blank project.
- Update the code in `/src/main.rs` file to add functionalities to the rust function, where the users can input a string and receive a reversed string.
- Add dependencies to `Cargo.toml` file, which includes tracing and tracing-subscriber.
- Run `cargo build --release` to build the application
- Run `cargo lambda deploy --region <region> --iam-role <IAM ROLE>` to deploy the rust lambda function you wrote. 
- After the lambda function is deployed to AWS cloud, you can go to the console and add a API Gateway for the corresponding lambda function. 
- Go to test console and input the desired format for you json input and check the X-Ray monitor.

## Screenshots
- Screenshot of logs printed in console: 
![Alt text](/screenshots/Log.png)
- Screenshot of X-Ray tracing enabled:
![Alt text](/screenshots/XRay.png)
- Screenshot a CloudWatch trace retrieved:
![Alt text](/screenshots/CloudWatch.png)
- Screenshot of a trace detail:
![Alt text](/screenshots/traceDetail.png)


